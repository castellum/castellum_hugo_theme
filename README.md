Very simple [Hugo](https://gohugo.io/) theme that only ships with 
[fontawesome](https://fontawesome.com/) icons as external dependency. It is used
at <https://castellum.mpib.berlin/>

It's inspired by the [Minimal Theme](https://github.com/calintat/minimal/).